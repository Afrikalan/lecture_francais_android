
/** Cree un objet de type Cellule; transmi par jclic (par opposition à une celulle de saisie de texte par exemple)
 * Un objet de type cellule doit avoir une méthode construire_affichage, et une méthode positionner (avec CSS cette méthode peut éventuellement être vide).
 */
function CelluleTransmise(donnees, objGrilleMere)
{
	var objCellule=this;
	objCellule.donnees=donnees;
	objCellule.objGrilleMere=objGrilleMere;
	objCellule.idReponse=objCellule.donnees.idReponse;
	objCellule.idUnique=objCellule.donnees.idUnique;
	objCellule.boolEstActive=true;
	objCellule.boolResolue=false;

	objCellule.construireAffichage=function(){
		var objCellule=this; //objCellule != this !!!
				      // ce pb se produit si un objet d'une classe appelle une méthode d'un autre objet d'une même classe. 
				      //Dans ce cas objCellule correspond à l'objet appelant et non à l'objet de la méthode appelée!!!
		objCellule.objJqCellule=$("<TD Class=\"Cellule Cell_"+objCellule.idUnique+"\"></TD>");
		if (typeof objCellule.donnees.image!="undefined")
			if (!objCellule.donnees.recouvrementTexteImage) $("<IMG SRC=\""+JqClic_Medias[objCellule.donnees.image].chemin+"\">").appendTo(objCellule.objJqCellule);
		if (typeof objCellule.donnees.texte!="undefined")
		{
			if (objCellule.donnees.texteEnPremier) $("<SPAN>"+objCellule.donnees.texte+"</SPAN>").prependTo(objCellule.objJqCellule);
			else $("<SPAN>"+objCellule.donnees.texte+"</SPAN>").appendTo(objCellule.objJqCellule);
		}
		objCellule.objJqCellule.click(objCellule.evtClicCell);
		objCellule.objJqCellule.bind("touchstart", objCellule.evtTouchStartCell);
		objCellule.objJqCellule.bind("touchend", objCellule.evtTouchEndCell);
		return(objCellule.objJqCellule);
	}

	
	//fonction appelee juste apres la construction de l'affichage, car certaines operations jouant sur le code CSS calculé nécéssitent que la celulle aie été insérer à sa place dans le DOM
	objCellule.construireAffichageFin=function(){
		if (typeof objCellule.donnees.image!="undefined")
			if (objCellule.donnees.recouvrementTexteImage)
				objCellule.definirImageFond(objCellule.donnees.image);
	}
	
	/**change la propriete background-image de la cellule. nb: celle-ci peut aussi contenir un dégradé!
	 * \param nomImage nom de l'image (identifiant dans la librairie). si false, retirer l'image
	 **/
	objCellule.definirImageFond=function(nomImage)
	{
	   //probleme: ne fonctionne pas si affichage pas construit...
		var nouvListeVal=[];
		var listeVal=objCellule.objJqCellule.css("background-image").split(",");
		
		if (nomImage!==false)
			nouvListeVal.push("url('"+JqClic_Medias[objCellule.donnees.image].chemin+"')");
		
		for (var i in listeVal)
			if ((! /^ ?url/i.test(listeVal[i])) && (listeVal[i]!="none"))
				nouvListeVal.push(listeVal[i]);
		
		objCellule.objJqCellule.css("background-image", nouvListeVal.join(", "));
	}
	
	objCellule.dimentionner=function(tailleAffX, tailleAffY, echelle)
	{
		//actuellement echelle non prise en compte, il faudra tester cela quand on aura bcp d'activités a convertir pour voir si cela convient et comment le calibrer, d'ici la......
		objCellule.objJqCellule.width(tailleAffX).height(tailleAffY);
		if (typeof objCellule.donnees.image!="undefined")
		{
			var zoomImgX=tailleAffX/JqClic_Medias[objCellule.donnees.image].tailleX;
			var zoomImgY=tailleAffY/JqClic_Medias[objCellule.donnees.image].tailleY;
			var zommImg=(Math.min(zoomImgX, zoomImgY, echelle));
			var nouvTailleX=Math.round(JqClic_Medias[objCellule.donnees.image].tailleX*zommImg);
			var nouvTailleY=Math.round(JqClic_Medias[objCellule.donnees.image].tailleY*zommImg);
			if (objCellule.donnees.recouvrementTexteImage) objCellule.objJqCellule.css("background-size", nouvTailleX+"px"+" "+nouvTailleY+"px");
			else objCellule.objJqCellule.find("img").width(nouvTailleX).height(nouvTailleY);
		}
	}
	
	objCellule.masquer=function()
	{
		objCellule.objJqCellule.css("visibility", "hidden").css("border", "none");
	}
	
	objCellule.remplacerContenuPar=function(objCellACopier)
	{
		var objCellNouvContenu=$.extend(true, {}, objCellACopier);
		objCellNouvContenu.objJqCellule=$("<TD Class=\"Cellule Cell_"+objCellule.idUnique+"\"></TD>");
		//objCellNouvContenu.juju="gege";
		
		objCellNouvContenu.donnees.recouvrementTexteImage=objCellule.donnees.recouvrementTexteImage; //l'image de fond reste en place, il faut donc éviter qu'une nouvelle image soit créé par dessus
		//objCell.construireAffichage().insertAfter(objCellule.objJqCellule);
		//objCellule.objJqCellule.remove();

		objCellule.objJqCellule.html(objCellNouvContenu.construireAffichage().html());
		
		if (typeof objCellNouvContenu.donnees.image!="undefined")
			if (objCellNouvContenu.donnees.recouvrementTexteImage) objCellule.objJqCellule.css("background-image", "url('"+JqClic_Medias[objCellule.donnees.image].chemin+"')");

// 		var proprietesACopier=["background-image", "text-align", "vertical-align"];
// 		for (var i in proprietesACopier)
// 			objCellule.objJqCellule.css(proprietesACopier[i], objCellNouvContenu.objJqCellule.css(proprietesACopier[i]))
	}
	
	objCellule.marquerResolue=function()
	{
		objCellule.objJqCellule.addClass("resolue");
		objCellule.boolResolue=true;
		
	}
	
	objCellule.marquerInactive=function()
	{
		objCellule.objJqCellule.addClass("inactive");
		objCellule.objJqCellule.css("background-image", "none");
		objCellule.objJqCellule.html("");
		objCellule.boolEstActive=false;
	}


	//
	// Evenements
	//

	objCellule.evtClicCell=function(evt)
	{
		window.gestionnaireLien.signalerClicCell(objCellule, evt);
		evt.stopPropagation();
	}
	
	objCellule.evtTouchStartCell=function(evt)
	{
		window.gestionnaireLien.signalerTouchStartCell(objCellule, evt);
		evt.stopPropagation();
	}

	objCellule.evtTouchEndCell=function(evt, boolCallback)
	{
		//Cas 1: L'évènement a été envoyé sur la bonne cellule
		if(typeof boolCallback !== "undefined")
			if(boolCallback===true)
			{
				window.gestionnaireLien.signalerTouchEndCell(objCellule);
				return(true);
			}

		//Cas 2: L'évènement a été envoyé sur la cellule originale, recherche de la cellule cible...
		var clicX=evt.originalEvent.changedTouches[0].pageX-$("body").scrollLeft()
		var clicY=evt.originalEvent.changedTouches[0].pageY-$("body").scrollTop()
		var eltDomCellCible=document.elementFromPoint(clicX, clicY);
		$(eltDomCellCible).trigger("touchend", [true]);
		//une autre solution serait de chercher l'id de la grille et de la cellule... et d'envoyer le bon objet
		//de cellule au gestionnaire de lien.
			
		//On annule le lien...
		window.gestionnaireLien.annuler();
		evt.stopPropagation();
	}
	
	objCellule.evtActivation=function()
	{
		var objJqSon;
		if (typeof objCellule.donnees.son!="undefined")
		{
		  //objSon=document.getElementById("son_"+objCellule.donnees.son);
		  //if(objSon!==null) objSon.play();
		  JqcGlob_gestionnaireSon.lireSon(objCellule.donnees.son);
		}
	}
	
	//
	// Gestion des réponses
	//
	
	//teste si deux cellules sont identiques, soit par leur ID, soit par leur contenu
	objCellule.comparerAvec=function(objCible)
	{
		//comparaison par ID
		if(objCellule.idReponse==objCible.idReponse) return(true);
		  
		//comparaison par contenu (comportement de JClic)
		if ((typeof objCellule.donnees.texte!="undefined") && (typeof objCible.donnees.texte!="undefined"))
			if ((objCellule.donnees.texte!="") && (objCible.donnees.texte!=""))
				if (objCellule.donnees.texte==objCible.donnees.texte)
					return (true);
		
		//les deux comparaisons ont échoué, les cellules sont différentes
		return (false);
	}
	
	///teste si un lien est correct entre deux cellules dans le cas d'une activité grille et si c'est le cas, renvoie un tableau associatif (objet) de type idGrille => objCellule
	///nb: cette fonction par du postulat que l'utilisation doit relier les réponses de deux grilles différentes. Elle ne fonctionnera donc pas pour les activités de type mémory, où il faut relier deux cellules de la même grille.
	/// \return si le lien est valide (bonne réponse) retourne un tableau de la forme idGrille->objCellule, si le lien n'est pas valide (mauvaise réponse) renvoie soit -1 (fausse réponse), soit -2 (la liaison entre ces deux cellules ne correspond pas à une réponse possible -> ne pas compter d'erreur)).
	objCellule.matchCellActGrille=function(objCible)
	{
		var arCells={};
		arCells[objCellule.objGrilleMere.id]=objCellule;
		arCells[objCible.objGrilleMere.id]=objCible;
	  
		if (objCible.objGrilleMere.id==objCellule.objGrilleMere.id) return(-2); //on ne peut matcher deux cellules appartenant à la même grille (on le fait pour les activités mémory, mais dans ce cas, il faudra utiliser directement la méthode comparerAvec).

		var arCellulesAComparer=arCells["primary"].objGrilleMere.arCellsParIdReponse(arCells["secondary"].idReponse); //on recherche toutes les cellules de la grille primary qui correspondent (via l'id) à la cellule de la grille secondaire cliquée
		
		//puis on compare la cellule de la grille primaire cliquée avec toutes ces cellules.
		for (var i in arCellulesAComparer)
			if(arCellulesAComparer[i].comparerAvec(arCells["primary"]))
				return(arCells);
		  
		return (-1); //réponse érronée
	}
}

///gere une grille de conteneurs de cellule
/// prop: objJqGrille Objet jquery représentant la grille
function AffichageGrille()
{
	var objAffGrille=this;
	//objAffGrille.idGrille="sans nom";
	objAffGrille.id="";
	objAffGrille.dimX=1;
	objAffGrille.dimY=1;
	objAffGrille.tailleCellX=1;
	objAffGrille.tailleCellY=1;

	objAffGrille.initialiser=function()
	{
		//pour la grille secondaire l'index de cellule correspond à son ID, alors que ce n'est pas le cas pour la grille primaire
		objAffGrille.arObjCellules=[];
	}
	
	
	/// renvoir 
	objAffGrille.arCellsParIdReponse=function(idReponse)
	{
		var arRetour=[];
		for (var i in objAffGrille.arObjCellules)
			if(objAffGrille.arObjCellules[i].idReponse==idReponse)
				arRetour.push(objAffGrille.arObjCellules[i]);

		return(arRetour);
	}
	
	objAffGrille.cellParIdUnique=function(idUnique)
	{
		for (var i in objAffGrille.arObjCellules)
			if(objAffGrille.arObjCellules[i].idUnique===idUnique)
				return(objAffGrille.arObjCellules[i]);

		return(false);
	}
	
	objAffGrille.objPosCellXYParIdUnique=function(idUnique)
	{
		var posDansArray=false;
	  
		for (var i in objAffGrille.arObjCellules)
			if(objAffGrille.arObjCellules[i].idUnique===idUnique)
				posDansArray=i;
				
		if (posDansArray===false) return(false);
		else return([posDansArray%objAffGrille.dimX, Math.floor(posDansArray/objAffGrille.dimX)]);
	}
	
	objAffGrille.arIdUniquesParOrdreCell=function()
	{
		var arRetour=[];
		for (var i in objAffGrille.arObjCellules)
			arRetour.push(objAffGrille.arObjCellules[i].idUnique);
		return(arRetour);
	}


	objAffGrille.construireAffichage=function(echelle)
	{
		var objLigne;

		objAffGrille.objJqGrille=$("<TABLE Class=\"Grille Grille_"+objAffGrille.id+"\"></TABLE>");
		for (var comptY=0; comptY<objAffGrille.dimY; comptY++)
		{
			objLigne=$("<TR></TR>").appendTo(objAffGrille.objJqGrille);
			for (var comptX=0; comptX<objAffGrille.dimX; comptX++)
				objAffGrille.arObjCellules[(comptY*objAffGrille.dimX)+comptX].construireAffichage().appendTo(objLigne);
		}
		
		return(objAffGrille.objJqGrille);
	}
	
	objAffGrille.construireAffichageFin=function(echelle) //operation d'affichage nécéssitant que l'object jclic soit déjà iniséré dans le DOM
	{
		for (var i in objAffGrille.arObjCellules)
			objAffGrille.arObjCellules[i].construireAffichageFin();
	}
	
	/** rempli la grille avec un tableau de cellule (éventuellement randomisé)
	 * 
	 * \param arCellTransmises tableau transmi par le script PHP
	 * \param rand true (mélanger) ou false (ne pas mélanger)
	 * 
	 */
	objAffGrille.peupler=function(arCellTransmises, rand)
	{
		if (rand) shuffle(arCellTransmises);
	  
		for(var i in arCellTransmises)
			objAffGrille.arObjCellules[i]=new CelluleTransmise(arCellTransmises[i], objAffGrille);
	}
	
	///Lit les parametres de la grille (ne cree pas de cellules)
	objAffGrille.hydrater=function(objParamGrille)
	{
		objAffGrille.id=objParamGrille.id;
		objAffGrille.dimX=objParamGrille.colonnes;
		objAffGrille.dimY=objParamGrille.lignes;
		objAffGrille.tailleCellX=objParamGrille.tailleCellX;
		objAffGrille.tailleCellY=objParamGrille.tailleCellY;
	}
	
	objAffGrille.tailleTheoriqueX=function()
	{
		return(objAffGrille.tailleCellX*objAffGrille.dimX);
	}

	objAffGrille.tailleTheoriqueY=function()
	{
		return(objAffGrille.tailleCellY*objAffGrille.dimY);
	}
	
	objAffGrille.dimentionner=function(echelle)
	{
		var tailleAffX=objAffGrille.tailleCellX*echelle;
		var tailleAffY=objAffGrille.tailleCellY*echelle;
		
		for (var i in objAffGrille.arObjCellules)
			objAffGrille.arObjCellules[i].dimentionner(tailleAffX, tailleAffY, echelle);
	}
	
	objAffGrille.toutDesactiver=function()
	{
		for (var i in objAffGrille.arObjCellules)
			objAffGrille.arObjCellules[i].marquerInactive();
	}
	
	///echange deux cellules: dans l'array interne 
	objAffGrille.echangerCellules=function(idCell1, idCell2, boolJqSwap)
	{
		var idx1=false;
		var idx2=false;
		
		for (var i in objAffGrille.arObjCellules)
		{
			if(objAffGrille.arObjCellules[i].idUnique===idCell1) idx1=i;
			if(objAffGrille.arObjCellules[i].idUnique===idCell2) idx2=i;
		}
		
		if ((idx1===false) || (idx2===false)) { alert("l'un des index ("+idCell1+", "+idCell2+") n'a pas été trouvé!"); return(false); }
	  
		var tmp=objAffGrille.arObjCellules[idx1];
		objAffGrille.arObjCellules[idx1]=objAffGrille.arObjCellules[idx2];
		objAffGrille.arObjCellules[idx2]=tmp;
		
		if(boolJqSwap)
		{
			var tmpJq=objAffGrille.arObjCellules[idx1].objJqCellule.clone().insertAfter(objAffGrille.arObjCellules[idx1].objJqCellule);
			objAffGrille.arObjCellules[idx1].objJqCellule.insertAfter(objAffGrille.arObjCellules[idx2].objJqCellule);
			objAffGrille.arObjCellules[idx2].objJqCellule.insertAfter(tmpJq);
			tmpJq.remove();
		}
	}
	
	objAffGrille.initialiser();
}


/**pour utiliser cet objet:
-contruire l'objet
-creer les grilles nécéssaires
-définir son layout
-contruire l'affichage
*/
function AffichageJeuGrilles()
{
	var objAffJeuGrilles=this;
	
	objAffJeuGrilles.initialiser=function()
	{
		objAffJeuGrilles.objJqFenetre=$("#Fenetre");
		objAffJeuGrilles.layout="AB";
	}
	
	objAffJeuGrilles.construireAffichage=function()
	{
		objAffJeuGrilles.objJqFenetre.html("");
		
		switch(objAffJeuGrilles.layout)
		{
		case "AB":
			objAffJeuGrilles.grilleA.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
 			objAffJeuGrilles.grilleB.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
		break;
		case "BA":
			objAffJeuGrilles.grilleB.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
 			objAffJeuGrilles.grilleA.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
		break;
		case "AUB":
			objAffJeuGrilles.grilleA.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
 			$("<BR>").appendTo(objAffJeuGrilles.objJqFenetre);
 			objAffJeuGrilles.grilleB.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
		break;
		case "BUA":
  			objAffJeuGrilles.grilleB.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
 			$("<BR>").appendTo(objAffJeuGrilles.objJqFenetre);
 			objAffJeuGrilles.grilleA.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
		break;
		case "A":
			objAffJeuGrilles.grilleA.construireAffichage().appendTo(objAffJeuGrilles.objJqFenetre);
		break;
		}
		
		switch(objAffJeuGrilles.layout)
		{
		case "AB":
		case "BA":
		case "AUB":
		case "BUA":
		   	objAffJeuGrilles.grilleA.construireAffichageFin();
  			objAffJeuGrilles.grilleB.construireAffichageFin();
		break;
		case "A":
			objAffJeuGrilles.grilleA.construireAffichageFin();
		break;
		}
		
		objAffJeuGrilles.dimentionner();
	}
	
	objAffJeuGrilles.tailleTheoriqueX=function()
	{
		switch(objAffJeuGrilles.layout)
		{
		case "AB":	
		case "BA":
			return(objAffJeuGrilles.grilleA.tailleTheoriqueX() + objAffJeuGrilles.grilleB.tailleTheoriqueX());
		break;
		case "AUB":
		case "BUA":
			return(Math.max(objAffJeuGrilles.grilleA.tailleTheoriqueX(), objAffJeuGrilles.grilleA.tailleTheoriqueX()));
		break;
		case "A":
			return(objAffJeuGrilles.grilleA.tailleTheoriqueX());
		break;
		}
	}

	objAffJeuGrilles.tailleTheoriqueY=function()
	{
		switch(objAffJeuGrilles.layout)
		{
		case "AB":	
		case "BA":
			return(Math.max(objAffJeuGrilles.grilleA.tailleTheoriqueY(), objAffJeuGrilles.grilleB.tailleTheoriqueY()));
		break;
		case "AUB":
		case "BUA":
			return(objAffJeuGrilles.grilleA.tailleTheoriqueY() + objAffJeuGrilles.grilleA.tailleTheoriqueY());
		break;
		case "A":
			return(objAffJeuGrilles.grilleA.tailleTheoriqueY());
		break;
		}
	}
	
	objAffJeuGrilles.dimentionner=function()
	{
		var echelle=1; //le calcul d'échelle en fonction de la taille de la fenetre se fera expérimentalement plus tardn quand on ara plus d'activités sur lesquelles faire des tests
		if (typeof objAffJeuGrilles.grilleA!="undefined") objAffJeuGrilles.grilleA.dimentionner(echelle);
		if (typeof objAffJeuGrilles.grilleB!="undefined") objAffJeuGrilles.grilleB.dimentionner(echelle);		
	}
	
	objAffJeuGrilles.initialiser();
}

//
// Gestion des liens
//
// Hooks:
// lienCell: en parametres les deux objets cellule ayant déclenché
//
function LienLigne()
{
	var objLien=this;
	objLien.WpCallHooks=WpCallHooks;
	objLien.WpAddHook=WpAddHook;
	
	objLien.initialiser=function()
	{
		objLien.lienEnCours=false;
		objLien.departX=0;
		objLien.departY=0;
	}
	
	//L'utilisateur tente d'initier un lien
	objLien.selectionCelluleDebut=function(objCellOrig, origX, origY)
	{
		if(!objCellOrig.boolEstActive) return(false); //une cellulle inactive ne peut initier un lien (mais elle peut le finir)
		if (typeof objCellOrig.idReponse!="undefined") if (objCellOrig.idReponse<0) return (false); //les celulles doivent contenir une réponse pour pouvoir être cliquables
		if (typeof objCellOrig.evtActivation!="undefined") objCellOrig.evtActivation();

		objLien.lienEnCours=true;
		JqcGlob_gestionnaireSon.lireSonEvt("click");
		objLien.objCellOrig=objCellOrig;
		objLien.departX=origX;
		objLien.departY=origY;
		objLien.objJqAffLien=$("<DIV CLASS=lien></DIV>").appendTo($("body")).offset({left:origX, top: origY});
	}
	
	//L'utilisateur tente de stopper un lien
	objLien.selectionCelluleFin=function(objCellFin)
	{
		objLien.annuler();
		if (typeof objCellFin.idReponse!="undefined") if (objCellFin.idReponse<0) return (false); //les celulles doivent contenir une réponse pour pouvoir être cliquables
		if (typeof objCellFin.evtActivation!="undefined") objCellFin.evtActivation();
		objLien.WpCallHooks("lienCell", objLien.objCellOrig, objCellFin);
	}
	
	objLien.signalerTouchStartCell=function(objCell, evt)
	{
		objLien.selectionCelluleDebut(objCell, evt.originalEvent.touches[0].pageX, evt.originalEvent.touches[0].pageY)
		window.addEventListener("touchmove", objLien.evtTouchMove);
	}
	
	objLien.signalerTouchEndCell=function(objCell, evt)
	{
		console.log(objCell); 
		objLien.selectionCelluleFin(objCell)
		window.removeEventListener("touchmove", objLien.evtTouchMove);
	}

	objLien.signalerClicCell=function(objCell, evt)
	{
		objLien.WpCallHooks("clicCell", objLien.objCellOrig, objCell);
	  
		if (objLien.lienEnCours)
		{
			objLien.selectionCelluleFin(objCell)	
			window.removeEventListener("mousemove", objLien.evtMouseMove);
		}
		else
		{
			objLien.selectionCelluleDebut(objCell, evt.pageX, evt.pageY)
			window.addEventListener("mousemove", objLien.evtMouseMove);
		}
	}
	
	//annuler et nettoie l'objet lien
	objLien.annuler=function()
	{
		if (typeof objLien.objJqAffLien!="undefined") objLien.objJqAffLien.remove();
		objLien.lienEnCours=false;
	}
	
	objLien.evtDeplPointeurLienActif=function(X, Y)
	{
		var deltaX=objLien.departX-X;
		var deltaY=objLien.departY-Y;
		var longeur=Math.sqrt(deltaX*deltaX+deltaY*deltaY);
		if(Math.abs(deltaX)>Math.abs(deltaY)) angle=(Math.acos(Math.abs(deltaX)/longeur)*180)/Math.PI;
		else angle=(Math.asin(Math.abs(deltaY)/longeur)*180)/Math.PI;
		if(deltaX*deltaY<0) angle=180-angle;
		if(deltaY>0) angle+=180;
		
		objLien.objJqAffLien.css("width", Math.round(longeur)+"px").css("transform", "rotate("+Math.round(angle*100)/100+"deg)");
	}
	
	objLien.evtMouseMove=function(evt) { objLien.evtDeplPointeurLienActif(evt.pageX, evt.pageY); }
	objLien.evtTouchMove=function(evt) { objLien.evtDeplPointeurLienActif(evt.touches[0].pageX, evt.touches[0].pageY); evt.preventDefault(); }
}

//
// Gestion des liens
//
// Hooks:
// lienCell: en parametres les deux objets cellule ayant déclenché
//
function LienDrag()
{ 
	var objLien=this;
	objLien.WpCallHooks=WpCallHooks;
	objLien.WpAddHook=WpAddHook;
	
	objLien.initialiser=function()
	{
		objLien.lienEnCours=false;
	}
	
	//L'utilisateur tente d'initier un lien
	objLien.selectionCelluleDebut=function(objCellOrig, origX, origY)
	{
		if(!objCellOrig.boolEstActive) return(false); //une cellulle inactive ne peut initier un lien (mais elle peut le finir)
		if (typeof objCellOrig.idReponse!="undefined") if (objCellOrig.idReponse<0) return (false); //les celulles doivent contenir une réponse pour pouvoir être cliquables
		if (typeof objCellOrig.evtActivation!="undefined") objCellOrig.evtActivation();
		objLien.lienEnCours=true;
		JqcGlob_gestionnaireSon.lireSonEvt("click");
		objLien.objCellOrig=objCellOrig;
		objLien.objJqCellOrig=objCellOrig.objJqCellule; 
		objLien.objJqCloneDrag=$("<DIV><TABLE><TR><TD></TD></TR></TABLE></DIV>").css("position", "absolute").css("pointer-events", "none").appendTo("#Fenetre").find("table").prop("class", objLien.objJqCellOrig.parent().parent().parent().prop("class")).find("tr td").prop("class", objLien.objJqCellOrig.prop("class")).attr("style", objLien.objJqCellOrig.attr("style")).html(objLien.objJqCellOrig.html()).width(objLien.objJqCellOrig.width()).height(objLien.objJqCellOrig.height()).end().end();
		objLien.objJqCellOrig.css("visibility", "hidden");
			
		var coordsCell=objLien.objJqCellOrig.offset();
		objLien.offsetX=origX-coordsCell.left;
		objLien.offsetY=origY-coordsCell.top;
	}
	
	objLien.selectionCelluleFin=function(objCellFin)
	{
		objLien.annuler();
		if (typeof objCellFin.idReponse!="undefined") if (objCellFin.idReponse<0) return (false); //les celulles doivent contenir une réponse pour pouvoir être cliquables
		if (typeof objCellFin.evtActivation!="undefined") objCellFin.evtActivation();
		objLien.WpCallHooks("lienCell", objLien.objCellOrig, objCellFin);
	}
	
	objLien.signalerTouchStartCell=function(objCell, evt)
	{
		objLien.selectionCelluleDebut(objCell, evt.originalEvent.touches[0].pageX, evt.originalEvent.touches[0].pageY)
		window.addEventListener("touchmove", objLien.evtTouchMove);
	}
	
	objLien.signalerTouchEndCell=function(objCell, evt)
	{
		objLien.selectionCelluleFin(objCell)
		window.removeEventListener("touchmove", objLien.evtTouchMove);
	}

	
	objLien.signalerClicCell=function(objCell, evt)
	{
		objLien.WpCallHooks("clicCell", objLien.objCellOrig, objCell);

		if (objLien.lienEnCours)
		{
			objLien.selectionCelluleFin(objCell);
		}
		else
		{
			objLien.selectionCelluleDebut(objCell, evt.pageX, evt.pageY);
			window.addEventListener("mousemove", objLien.evtMouseMove);
			objLien.objJqCloneDrag.offset({left: evt.pageX-objLien.offsetX, top: evt.pageY-objLien.offsetY});
		}
	}
	
	//annuler et nettoie l'objet lien
	objLien.annuler=function()
	{
		window.removeEventListener("mousemove", objLien.evtMouseMove);
		if (typeof objLien.objJqCloneDrag!="undefined") objLien.objJqCloneDrag.remove();
		objLien.lienEnCours=false;
		if (typeof objLien.objJqCellOrig!="undefined") objLien.objJqCellOrig.css("visibility", "visible");
	}

	objLien.evtDeplPointeurLienActif=function(X, Y)
	{
		objLien.objJqCloneDrag.offset({left: X-objLien.offsetX, top: Y-objLien.offsetY});
	}
	
	objLien.evtMouseMove=function(evt) { objLien.evtDeplPointeurLienActif(evt.pageX, evt.pageY); }
	objLien.evtTouchMove=function(evt) { objLien.evtDeplPointeurLienActif(evt.touches[0].pageX, evt.touches[0].pageY); evt.preventDefault(); }

}
