

// Gestion générale des activités

function Activite(id_activite)
{
	var objActivite=this;
	objActivite.id_activite=id_activite;
	objActivite.donnesActivite=$.extend(true, {}, eval("JqClic_Activite_"+objActivite.id_activite));
	objActivite.WpAddHook=WpAddHook;
	objActivite.WpCallHooks=WpCallHooks;
  objActivite.prete=false;

	objActivite.initialiser=function()
	{
		//nettoyage
		if(typeof window.gestionnaireLien!="undefined") window.gestionnaireLien.annuler();
		
		//initilisation	  
		$("body").prop("class", objActivite.id_activite);
		JqcGlob_gestionnaireSon.integrerSonsActivite(objActivite.donnesActivite.sonsEvenements);
		JqcGlob_gestionnaireSon.lireSonEvt("start");
	  
		objActivite.WpCallHooks("initialiser");
	}
	
	objActivite.afficher=function()
	{
		objActivite.WpCallHooks("afficher");
		window.document.title="JQClic ["+objActivite.id_activite+"]";
	}
	
	objActivite.evtBonneReponse=function()
	{
		JqcGlob_gestionnaireSon.lireSonEvt("actionOk");
	}
	
	objActivite.evtMauvaiseReponse=function()
	{
		JqcGlob_gestionnaireSon.lireSonEvt("actionError");
	}
	
	objActivite.evtActiviteReussie=function()
	{
		JqcGlob_gestionnaireSon.lireSonEvt("finishedOk");
		JqcGlob_Sequence.evtActiviteReussie();
	}
	
	objActivite.evtActiviteEchouee=function()
	{
		JqcGlob_gestionnaireSon.lireSonEvt("finishedError");
	}
	
	//charge les extentions propres au type d'activités
	switch(objActivite.donnesActivite.type)
	{
	case "JClic:@associations.SimpleAssociation":
	case "JClic:@associations.ComplexAssociation":
		Activite_Asso(objActivite);
    objActivite.prete=true;
	break;
	case "JClic:@puzzles.DoublePuzzle":
		Activite_PuzzleDouble(objActivite);
    objActivite.prete=true;
	break;
	case "JClic:@puzzles.ExchangePuzzle":
		Activite_PuzzleEch(objActivite);
    objActivite.prete=true;
	break;
	case "JClic:@puzzles.HolePuzzle":
		Activite_PuzzleTrou(objActivite);
    objActivite.prete=true;
	break;
	case "JClic:@memory.MemoryGame":
		Activite_Memory(objActivite);
    objActivite.prete=true;
	break;
	default:
		console.log("type d'activite "+objActivite.donnesActivite.type+" non pris en charge");
	}
	
	if (objActivite.prete)
    objActivite.initialiser();
}

//
// ***************************************
// ***************************************
// **					**
// ** 	     ACTIVITES A GRILLES	**
// **					**
// ***************************************
// ***************************************
//
// Gestion des activités à grille ces classes étendent la classe Activité de base


//
// ****************************************
// * ACTIVITES D'ASSOCIATION
// ****************************************
//

//gestion d'une activité d'association (simple ou complexe)
function Activite_Asso(objActivite)
{
	objActivite.WpAddHook("initialiser", function(){
		objActivite.jeuGrilles=new AffichageJeuGrilles();
		objActivite.jeuGrilles.layout=objActivite.donnesActivite.layout;
		objActivite.jeuGrilles.grilleA=new AffichageGrille();
		objActivite.jeuGrilles.grilleA.hydrater(objActivite.donnesActivite.grilles.primary);
		objActivite.jeuGrilles.grilleA.peupler(objActivite.donnesActivite.grilles.primary.cellules, objActivite.donnesActivite.grilles.primary.melanger);
		objActivite.jeuGrilles.grilleB=new AffichageGrille();
		objActivite.jeuGrilles.grilleB.hydrater(objActivite.donnesActivite.grilles.secondary);
		objActivite.jeuGrilles.grilleB.peupler(objActivite.donnesActivite.grilles.secondary.cellules, objActivite.donnesActivite.grilles.primary.melanger);
		if (objActivite.donnesActivite.type_lien=="ligne") window.gestionnaireLien=new LienLigne();
		else if (objActivite.donnesActivite.type_lien=="drag") window.gestionnaireLien=new LienDrag(); else alert("!!! Type de lien inconnu !!!");
		window.gestionnaireLien.WpAddHook("lienCell", objActivite.hkLienCell);
		
		if (typeof objActivite.donnesActivite.grilles.solvedPrimary!="undefined")
		{
			objActivite.boolGrilleAlt=true;
			objActivite.objGrilleAlt=new AffichageGrille();
			objActivite.objGrilleAlt.hydrater(objActivite.donnesActivite.grilles.solvedPrimary, false);
			objActivite.objGrilleAlt.peupler(objActivite.donnesActivite.grilles.solvedPrimary.cellules);
			//objActivite.objGrilleAlt.construireAffichage();

		} else objActivite.boolGrilleAlt=false;
		
	});

	objActivite.WpAddHook("afficher", function(){
		objActivite.jeuGrilles.construireAffichage();
	});
	
	objActivite.testSiComplette=function()
	{
		for(var i in objActivite.jeuGrilles.grilleA.arObjCellules)
		{
			var idReponse=objActivite.jeuGrilles.grilleA.arObjCellules[i].idReponse;
			var boolResolue=objActivite.jeuGrilles.grilleA.arObjCellules[i].boolResolue;
			if ((! boolResolue) && (objActivite.jeuGrilles.grilleB.arCellsParIdReponse(idReponse).length>0)) //cellule non résolue dans la grille primaire + réponse correspondate dans la grille secondaire=l'utilisateur doit encore au moins répondre pour cette case, donc activité pas finie.
				return(false);
		}
		
		return(true);
	}
	
	objActivite.hkLienCell=function(cell1, cell2){
		var resultAnalyseReponse=cell1.matchCellActGrille(cell2);
		
		if (typeof resultAnalyseReponse=="object")
		{
			objActivite.evtBonneReponse();
			resultAnalyseReponse["primary"].marquerResolue();

			if (objActivite.boolGrilleAlt)
			{
				var cellAlt;
				if (cellAlt=objActivite.objGrilleAlt.cellParIdUnique(resultAnalyseReponse["primary"].idUnique))
					resultAnalyseReponse["primary"].remplacerContenuPar(cellAlt);
				else resultAnalyseReponse["primary"].masquer();
			} else /*resultAnalyseReponse["primary"].masquer();*/ resultAnalyseReponse["primary"].marquerInactive();
			if (objActivite.testSiComplette()) objActivite.evtActiviteReussie();
		}
		else if(resultAnalyseReponse===-1)
			objActivite.evtMauvaiseReponse();
		else if(resultAnalyseReponse===-2)
		{ /* réponse ne correspondant à rien, on ne fait rien */ }
	}
}

//
// ****************************************
// * ACTIVITES DE PUZZLE
// ****************************************
//

function Activite_PuzzleDouble(objActivite)
{
	objActivite.WpAddHook("initialiser", function(){
		var donneesGrilleB=$.extend(true, {}, objActivite.donnesActivite.grilles.primary, {id: "secondary"}); //le true au debut délenche une copie récursive
		objActivite.jeuGrilles=new AffichageJeuGrilles();
		objActivite.jeuGrilles.layout=objActivite.donnesActivite.layout;
		objActivite.jeuGrilles.grilleA=new AffichageGrille();
		objActivite.jeuGrilles.grilleA.hydrater(objActivite.donnesActivite.grilles.primary);
		objActivite.jeuGrilles.grilleA.peupler(objActivite.donnesActivite.grilles.primary.cellules, objActivite.donnesActivite.grilles.primary.melanger);
		objActivite.jeuGrilles.grilleB=new AffichageGrille();
		objActivite.jeuGrilles.grilleB.hydrater(donneesGrilleB);
		objActivite.jeuGrilles.grilleB.peupler(donneesGrilleB.cellules, false);

		if (objActivite.donnesActivite.type_lien=="ligne") window.gestionnaireLien=new LienLigne();
		else if (objActivite.donnesActivite.type_lien=="drag") window.gestionnaireLien=new LienDrag(); else alert("!!! Type de lien inconnu !!!");
		window.gestionnaireLien.WpAddHook("lienCell", objActivite.hkLienCell);
		
		/*if (typeof objActivite.donnesActivite.grilles.solvedPrimary!="undefined")
		{
			objActivite.boolGrilleAlt=true;
			objActivite.objGrilleAlt=new AffichageGrille();
			objActivite.objGrilleAlt.hydrater(objActivite.donnesActivite.grilles.solvedPrimary, false);
			objActivite.objGrilleAlt.peupler(objActivite.donnesActivite.grilles.solvedPrimary.cellules);
			//objActivite.objGrilleAlt.construireAffichage();

		} else */objActivite.boolGrilleAlt=false;
		
	});

	objActivite.WpAddHook("afficher", function(){
		objActivite.jeuGrilles.construireAffichage();
		objActivite.jeuGrilles.grilleB.objJqGrille.removeClass("Grille_secondary").addClass("Grille_primary");
		objActivite.jeuGrilles.grilleB.toutDesactiver();
	});
	
	objActivite.testSiComplette=function()
	{
		for(var i in objActivite.jeuGrilles.grilleA.arObjCellules)
		{
			var idReponse=objActivite.jeuGrilles.grilleA.arObjCellules[i].idReponse;
			var boolResolue=objActivite.jeuGrilles.grilleA.arObjCellules[i].boolResolue;
			if ((! boolResolue) && (objActivite.jeuGrilles.grilleB.arCellsParIdReponse(idReponse).length>0)) //cellule non résolue dans la grille primaire + réponse correspondate dans la grille secondaire=l'utilisateur doit encore au moins répondre pour cette case, donc activité pas finie.
				return(false);
		}
		
		return(true);
	}
	
	objActivite.hkLienCell=function(cell1, cell2){
		var resultAnalyseReponse=cell1.matchCellActGrille(cell2);
		
		if (typeof resultAnalyseReponse=="object")
		{
			objActivite.evtBonneReponse();
			resultAnalyseReponse["primary"].objJqCellule.juju=1;
			resultAnalyseReponse["secondary"].objJqCellule.juju=2;
			
			resultAnalyseReponse["secondary"].remplacerContenuPar(resultAnalyseReponse["primary"]);
			resultAnalyseReponse["primary"].marquerResolue();
			resultAnalyseReponse["primary"].marquerInactive();

			;
			
			
			if (objActivite.testSiComplette()) objActivite.evtActiviteReussie();
		}
		else if(resultAnalyseReponse===-1)
			objActivite.evtMauvaiseReponse();
		else if(resultAnalyseReponse===-2)
		{ /* réponse ne correspondant à rien, on ne fait rien */ }
	}
}

function Activite_PuzzleEch(objActivite)
{
	objActivite.WpAddHook("initialiser", function(){
		objActivite.jeuGrilles=new AffichageJeuGrilles();
		objActivite.jeuGrilles.layout="A";
		objActivite.jeuGrilles.grilleA=new AffichageGrille();
		objActivite.jeuGrilles.grilleA.hydrater(objActivite.donnesActivite.grilles.primary);
		objActivite.jeuGrilles.grilleA.peupler(objActivite.donnesActivite.grilles.primary.cellules, objActivite.donnesActivite.grilles.primary.melanger);

		if (objActivite.donnesActivite.type_lien=="ligne") window.gestionnaireLien=new LienLigne();
		else if (objActivite.donnesActivite.type_lien=="drag") window.gestionnaireLien=new LienDrag(); else alert("!!! Type de lien inconnu !!!");
		window.gestionnaireLien.WpAddHook("lienCell", objActivite.hkLienCell);
		
		objActivite.boolGrilleAlt=false;
	});

	objActivite.WpAddHook("afficher", function(){
		objActivite.jeuGrilles.construireAffichage();
	});
	
	objActivite.testSiComplette=function()
	{
		for(var i in objActivite.jeuGrilles.grilleA.arObjCellules)
		{
			var idReponse=objActivite.jeuGrilles.grilleA.arObjCellules[i].idReponse;
			if (idReponse!=i) return(false);
		}
		
		return(true);
	}
	
	objActivite.hkLienCell=function(cell1, cell2){
		objActivite.evtBonneReponse();
		objActivite.jeuGrilles.grilleA.echangerCellules(cell1.idUnique, cell2.idUnique, true);
		if (objActivite.testSiComplette()) objActivite.evtActiviteReussie();
	}
}

//
// Activité puzzle à trou type taquin
//
function Activite_PuzzleTrou(objActivite)
{
	objActivite.WpAddHook("initialiser", function(){
	  
		
  		objActivite.idUniqueCellTrou=Math.floor(Math.random()*(objActivite.donnesActivite.grilles.primary.lignes*objActivite.donnesActivite.grilles.primary.colonnes));
		var donneesGrilleB={id: "secondary", colonnes: 1, lignes: 1, melanger: false, 
		cellules: [$.extend(true, {}, objActivite.donnesActivite.grilles.primary.cellules[objActivite.idUniqueCellTrou])],
		tailleCellX: objActivite.donnesActivite.grilles.primary.tailleCellX, tailleCellY: objActivite.donnesActivite.grilles.primary.tailleCellY};
	  
		objActivite.jeuGrilles=new AffichageJeuGrilles();
		objActivite.jeuGrilles.layout=objActivite.donnesActivite.layout;
		objActivite.jeuGrilles.grilleA=new AffichageGrille();
		objActivite.jeuGrilles.grilleA.hydrater(objActivite.donnesActivite.grilles.primary);
		objActivite.jeuGrilles.grilleA.peupler(objActivite.donnesActivite.grilles.primary.cellules, objActivite.donnesActivite.grilles.primary.melanger);
		objActivite.jeuGrilles.grilleB=new AffichageGrille();
		objActivite.jeuGrilles.grilleB.hydrater(donneesGrilleB);
		objActivite.jeuGrilles.grilleB.peupler(donneesGrilleB.cellules, false);

		var arPositionPourVerifSolvable=[];
		for (var i in objActivite.jeuGrilles.grilleA.arObjCellules)
			arPositionPourVerifSolvable.push(objActivite.jeuGrilles.grilleA.arObjCellules[i].idUnique+1);
		if (!(objActivite.testTaquinSolvable(arPositionPourVerifSolvable, objActivite.idUniqueCellTrou+1, objActivite.donnesActivite.grilles.primary.colonnes, objActivite.donnesActivite.grilles.primary.lignes)))
		{
			var arCasesInversablePourCorrection=[];
			for (var i in objActivite.jeuGrilles.grilleA.arObjCellules)
				if (objActivite.jeuGrilles.grilleA.arObjCellules[i].idUnique!=objActivite.idUniqueCellTrou)
					arCasesInversablePourCorrection.push(objActivite.jeuGrilles.grilleA.arObjCellules[i].idUnique);
			objActivite.jeuGrilles.grilleA.echangerCellules(arCasesInversablePourCorrection[0], arCasesInversablePourCorrection[1], false);
		}
		
		window.gestionnaireLien=new objActivite.gestionnaireLien_PuzzleTaquin(objActivite.idUniqueCellTrou);
		window.gestionnaireLien.WpAddHook("lienCell", objActivite.hkLienCell);

		objActivite.boolGrilleAlt=false;
		
	});

	objActivite.WpAddHook("afficher", function(){
		objActivite.jeuGrilles.construireAffichage();
		objActivite.jeuGrilles.grilleB.objJqGrille.removeClass("Grille_secondary").addClass("Grille_primary");
		objActivite.jeuGrilles.grilleA.cellParIdUnique(objActivite.idUniqueCellTrou).marquerInactive();
	});
	
	objActivite.testSiComplette=function()
	{
		for(var i in objActivite.jeuGrilles.grilleA.arObjCellules)
		{
			var idReponse=objActivite.jeuGrilles.grilleA.arObjCellules[i].idReponse;
			if (idReponse!=i) return(false);
		}
		
		return(true);
	}
	
	


	objActivite.testTaquinSolvable=function(arPositions, idVide, tailleX, tailleY)
	{
		var permutsNecessaires=0;
		var delta=1; //permet de gérer que la numérotation des cellules commence à 1;
		var tmpPermut;
		var posValeur;

		//position de la case vide avant permutation.
		var posVide=arPositions.indexOf(idVide);
		var posVideX=posVide%tailleX;
		var posVideY=Math.floor(posVide/tailleX);
		
		var posVideDest=idVide-1;
		var posVideDestX=posVideDest%tailleX;
		var posVideDestY=Math.floor(posVideDest/tailleX);
		
		var depsVideNecessaires=Math.abs(posVideDestX-posVideX)+Math.abs(posVideDestY-posVideY);

		//calcul du nombre de permutations pour remetre dans l'ordre
		for (var compteur=0; compteur<arPositions.length; compteur++)
			if(arPositions[compteur]!=compteur+delta)
			{
				posValeur=arPositions.indexOf(compteur+delta);
				if (posValeur==-1) { alert("Element "+(compteur+delta)+" inexistant!!!"); return(false); }
				tmpPermut=arPositions[compteur];
				arPositions[compteur]=arPositions[posValeur];
				arPositions[posValeur]=tmpPermut;
				permutsNecessaires++;
			}
			
		if(depsVideNecessaires%2==permutsNecessaires%2)
			return (true);
		else
			return (false);
		
	}
	
	objActivite.hkLienCell=function(cell1, cell2){
		objActivite.evtBonneReponse();
		objActivite.jeuGrilles.grilleA.echangerCellules(cell1.idUnique, cell2.idUnique, true);
		if (objActivite.testSiComplette()) objActivite.evtActiviteReussie();
	}

	//gestionnaire de lien spécifique aux activités de taquin.
	//reçoit l'objet activité (taquin) l'ayant appelé et interagit spécifiquement avec cet objet.
	objActivite.gestionnaireLien_PuzzleTaquin=function(idUniqueCellTrou)
	{
		var gestionnaireLien=this;
		gestionnaireLien.WpCallHooks=WpCallHooks;
		gestionnaireLien.WpAddHook=WpAddHook;
		gestionnaireLien.idUniqueCellTrou=idUniqueCellTrou;
		
		gestionnaireLien.initialiser=function()
		{
			
		}
		
		gestionnaireLien.signalerClicCell=function(cell)
		{
			var positionCellTrou=objActivite.jeuGrilles.grilleA.objPosCellXYParIdUnique(gestionnaireLien.idUniqueCellTrou);
			var positionCellClic=objActivite.jeuGrilles.grilleA.objPosCellXYParIdUnique(cell.idUnique);
			
			var distance=Math.abs(positionCellTrou[0]-positionCellClic[0])+Math.abs(positionCellTrou[1]-positionCellClic[1]);
			if (distance==1) gestionnaireLien.WpCallHooks("lienCell", cell, objActivite.jeuGrilles.grilleA.cellParIdUnique(gestionnaireLien.idUniqueCellTrou));
		}
		
		gestionnaireLien.annuler=function(){}
		
		gestionnaireLien.initialiser();
	}
}

function Activite_Memory(objActivite)
{
	objActivite.WpAddHook("initialiser", function(){
		
		var donneesAct=$.extend(true, {}, objActivite.donnesActivite);
		
		if (typeof objActivite.donnesActivite.grilles.solvedPrimary!="undefined")
			for (var i in objActivite.donnesActivite.grilles.primary.cellules) //copie de la grille A une autre fois.
				donneesAct.grilles.primary.cellules.push($.extend(true, {}, objActivite.donnesActivite.grilles.primary.cellules[i]));
		else  
			for (var i in objActivite.donnesActivite.grilles.primary.cellules) //deuxième copie de la mleme grille
				donneesAct.grilles.primary.cellules.push($.extend(true, {}, objActivite.donnesActivite.grilles.primary.cellules[i]));
		
		switch(objActivite.donnesActivite.layout)
		{
		case "AB":
		case "BA":
			donneesAct.grilles.primary.colonnes*=2;
		break;
		case "AUB":
		case "BUA":
			donneesAct.grilles.primary.lignes*=2;
		break;
		}

		objActivite.jeuGrilles=new AffichageJeuGrilles()
		objActivite.jeuGrilles.layout="A";
		objActivite.jeuGrilles.grilleA=new AffichageGrille();
		objActivite.jeuGrilles.grilleA.hydrater(donneesAct.grilles.primary);
		objActivite.jeuGrilles.grilleA.peupler(donneesAct.grilles.primary.cellules, donneesAct.grilles.primary.melanger);

		if (objActivite.donnesActivite.type_lien=="ligne") window.gestionnaireLien=new LienLigne();
		else if (objActivite.donnesActivite.type_lien=="drag") window.gestionnaireLien=new LienDrag(); else alert("!!! Type de lien inconnu !!!");
		window.gestionnaireLien.WpAddHook("lienCell", objActivite.hkLienCell);
		window.gestionnaireLien.WpAddHook("clicCell", objActivite.hkClicCell);
		
		objActivite.boolGrilleAlt=false; //grille alt intergrée a grille A si présetne donc pas gérée comme telle...
		
	});

	objActivite.WpAddHook("afficher", function(){
		objActivite.jeuGrilles.construireAffichage();
		objActivite.jeuGrilles.grilleA.toutDesactiver();
	});
	
	objActivite.testSiComplette=function()
	{
		for(var i in objActivite.jeuGrilles.grilleA.arObjCellules)
		{
			var idReponse=objActivite.jeuGrilles.grilleA.arObjCellules[i].idReponse;
			var boolResolue=objActivite.jeuGrilles.grilleA.arObjCellules[i].boolResolue;
			if ((! boolResolue) && (objActivite.jeuGrilles.grilleB.arCellsParIdReponse(idReponse).length>0)) //cellule non résolue dans la grille primaire + réponse correspondate dans la grille secondaire=l'utilisateur doit encore au moins répondre pour cette case, donc activité pas finie.
				return(false);
		}
		
		return(true);
	}
	
	objActivite.hkClicCell=function(cell)
	{
	}
	
	objActivite.hkLienCell=function(cell1, cell2){
		var resultAnalyseReponse=cell1.matchCellActGrille(cell2);
		
		if (typeof resultAnalyseReponse=="object")
		{
			objActivite.evtBonneReponse();
			resultAnalyseReponse["primary"].marquerResolue();

			if (objActivite.boolGrilleAlt)
			{
				var cellAlt;
				if (cellAlt=objActivite.objGrilleAlt.cellParIdUnique(resultAnalyseReponse["primary"].idUnique))
					resultAnalyseReponse["primary"].remplacerContenuPar(cellAlt);
				else resultAnalyseReponse["primary"].masquer();
			} else /*resultAnalyseReponse["primary"].masquer();*/ resultAnalyseReponse["primary"].marquerInactive();
			if (objActivite.testSiComplette()) objActivite.evtActiviteReussie();
		}
		else if(resultAnalyseReponse===-1)
			objActivite.evtMauvaiseReponse();
		else if(resultAnalyseReponse===-2)
		{ /* réponse ne correspondant à rien, on ne fait rien */ }
	}
}
