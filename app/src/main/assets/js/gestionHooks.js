/*
 * Systeme de gestion des hooks/events javascripts
 * 
 * 2 fonctions (globales)
 * 
 * 
 * Fonction WpAddHook(hookNom, refFctHook, politesse)
 * 
 * 	Description: Cette fonction permet d'ajouter un hook (action permetant a un plugin de declarer qu'il souhaite acceder a un point d'entrée donné d'un programme)
 * 	hookNom: identifiant du hook
 * 	refFctHook: reference vers la fonction (contrairement à la version php ou c'est un nom de fonction qui est passé)
 * 	politesse: plus cette valeur est élevée, moins cette fonction sera prioritaire en cas de plusieurs fonctions sur le meme hook
 * 
 * 	Pas de valeur retour
 * 
 * 
 * 
 * Fonction WpCallHooks(hookNom, hookArgument1, hookArgumentN)
 * 
 * 	Description: Cette fonction sert à appeler un hook (donc a mettre en place un point d'entrée pour plugins
 * 
 * 	hookNom: identifiant du hook
 * 	hookArgument1, ... , hookArgumentN: arguments à transmettre a la fonction, s'il y en a
 * 
 * 	Valeur retour: Comme la version php, un array avec les valeur de retour des différentes fonction (a parcour avec for i in...)
 */



function WpAddHook(hookNom, refFctHook, politesse, options) 
{    
	var insere=false;
	if (typeof options=="undefined")
		options={};
	
	if (typeof this.WpHooks=="undefined") this.WpHooks=new Object();
	//ici this fait reference soit a l'element window (hook global), soit a une instance d'objet
  
	if (typeof politesse=="undefined") politesse=1000;
	
	if (typeof this.WpHooks[hookNom]=="undefined")
		this.WpHooks[hookNom]=new Array();
	
	var maxidhook=0;
	for (var i in this.WpHooks[hookNom])
		if (this.WpHooks[hookNom][i].id>maxidhook)
			maxid=this.WpHooks[hookNom][i].id;

	for (var i in this.WpHooks[hookNom])
		if (this.WpHooks[hookNom][i].politesse>politesse) //si on trouve un elt moins prioritaire, on s'insere avant
		{
			this.WpHooks[hookNom].splice(i, 0, $.merge({}, options, {refFctHook: refFctHook, politesse: politesse, id: maxidhook+1}));
			insere=true;
			break;
		}
	
	if (!insere)
		this.WpHooks[hookNom].push({refFctHook: refFctHook, politesse: politesse}); //Sinon, insertion a la fin.
		
	return(maxidhook+1);
}


function WpDelHook(hookNom, id)
{
	var maxidhook=0;
	for (var i in this.WpHooks[hookNom])
		if (this.WpHooks[hookNom][i].id==id)
		{
			this.WpHooks[hookNom].splice(i, 1);
			return (true);
		}
	return(false);
}


function WpCallHooks(hookNom, hookArgument1, hookArgument2, hookArgument3, hookArgument4, hookArgument5, hookArgument6, hookArgument7, hookArgument8, hookArgument9, hookArgument10) 
{  
	if (typeof this.WpHooks=="undefined") this.WpHooks=new Object();
  
  // On regarde si des fonctions sont enregistrées à notre Hook  
	var refFctHook;
	
	var result=new Array()
	
	if (typeof this.WpHooks[hookNom]!="undefined")
		for (var i in this.WpHooks[hookNom])
		{
			refFctHook=this.WpHooks[hookNom][i].refFctHook;
			
			if(typeof this.WpHooks[hookNom][i].usageUnique!="undefined")
				if(this.WpHooks[hookNom][i].usageUnique)
					this.WpDelHook(hookNom, i);
			//console.log(hookNom);
			result.push(refFctHook(hookArgument1, hookArgument2, hookArgument3, hookArgument4, hookArgument5, hookArgument6, hookArgument7, hookArgument8, hookArgument9, hookArgument10));
		}
			
	return(result);
}  
